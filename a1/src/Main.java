import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //Name
        System.out.println("First Name:");
        String firstName = scanner.nextLine();
        System.out.println("Last Name:");
        String lastName = scanner.nextLine();

        //Grades
        System.out.println("First Subject Grade:");
        double firstSubject = scanner.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondSubject = scanner.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdSubject = scanner.nextDouble();
        double ave = (firstSubject + secondSubject + thirdSubject)/3;


        //Print
        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + ave);


    }
}