//import java.util.Scanner; -- this is manually typing to import

import java.util.Scanner; // Alt+Enter Scanner below to automatically import

/*
    Main Class

    The Main class is the entry point for our Java program.
    It is responsible for executing our code.
    The main class usually has 1 method inside it, the main() method.
    Not every project has a main class?

    ctrl + / = single comment
    ctrl + shift + / = multi-line comments

*/


public class Main {
    public static void main(String[] args) {
        System.out.println("Elaine.");
        System.out.println("I am learning Java.");

        /*
            Main Method is where the most executable code is applied to.

            "public" is an access modifier which simply tells the application which classes have access to our method/attributes

            "static" means that the method/property belongs to the class. This means that it is accessible without having to create an instance of a class.

            "void" - In Java, we have to declare the data type of the method's return and since main method is usually used as an entry point, it should not return data, thus we add void.

            "sout" -> system.out.printIn() - prints the value of the argument passed
        */

        int myNum;
        myNum = 3;
        System.out.println(myNum);

        //myNum = "sampleString";

        myNum = 2500;
        System.out.println(myNum);

        int nationalPopulation = 2147483647;
        System.out.println(nationalPopulation);

        // "L" is also added at the end (on top of initializing it to "long" data type) for it to be fully recognized as a long number
        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        // "f" is also added at the end (on top of initializing it to float data type) for it to be fully recognized as a "float" number
        float piFloat = 3.1415926f;
        System.out.println(piFloat);

        // ' ' is for char , " " is for string
        char letter = 'a';


        /*
            Strings
                - non-primitive data-type (in Java)
                - non-primitive data types have access to methods: Array,Class,Interface

            .isEmpty()
                - is a string method that checks length of string
                - returns a boolean; true if length = 0, otherwise false

            Scanner
                - is a class that creates an instance that accepts inputs from the terminal.
                - similar to prompt() in JS
                - being a class pre-defined by Java, it has to be imported

            --- These receive user input and returns: ---

            .nextLine()         String

            .nextInt()          integer type

            .nextDouble()       double type

        */

        String username = "";
        System.out.println(username.isEmpty());

        Scanner scannerName = new Scanner(System.in);

        //String
        System.out.println("What is your name?");
        String myName = scannerName.nextLine();
        System.out.println("My name is " + myName);

        //int
        System.out.println("What is your favourite number?");
        int myFaveNum = scannerName.nextInt();
        System.out.println(myFaveNum);

        //double
        System.out.println("What was your average grade in HS?");
        double aveGrade = scannerName.nextDouble();
        System.out.println(aveGrade);

        //addition - sum
        System.out.println("Enter the first number:");
        int number1 = scannerName.nextInt();
        System.out.println("Enter the second number:");
        int number2 = scannerName.nextInt();

        int sum = number1 + number2;
        System.out.println("The sum of both numbers is: " + sum);

        System.out.println("Enter the first number:");
        int number3 = scannerName.nextInt();
        System.out.println("Enter the second number:");
        int number4 = scannerName.nextInt();

        int diff = number1 - number2;
        System.out.println("The difference is: " + diff);

    }
}

